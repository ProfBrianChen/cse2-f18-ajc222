public class Arithmetic
{
	
	public static void main (String[] args)
	{
		//  Assign our variable values

		int numPants = 3;
		double pantsPrice = 34.98;
    int numShirts = 2;
    double shirtPrice = 24.99;
    int numBelts = 1;
    double beltPrice = 33.99;
    double paSalesTax = 0.06;	

// Calculations

    double pantsCost = (numPants * pantsPrice);
    double shirtCost = (numShirts * shirtPrice);
    double beltCost = (numBelts * beltPrice);
    
    double pantsTax = pantsCost * paSalesTax;  // 6.4343
    pantsTax *= 100;  // 643.43
    int pantsTaxRound = (int) pantsTax; // 643
    double pantsTaxRound2 = (double) pantsTaxRound; //643.00
    pantsTaxRound2 /= 100; // 6.43
    pantsTax = pantsTaxRound2;
    
    double shirtTax = shirtCost * paSalesTax;  // 6.4343
    shirtTax *= 100;  // 643.43
    int shirtTaxRound = (int) shirtTax; // 643
    double shirtTaxRound2 = (double) shirtTaxRound; //643.00
    shirtTaxRound2 /= 100; // 6.43
    shirtTax = shirtTaxRound2;
    
    double beltTax = beltCost * paSalesTax;  // 6.4343
    beltTax *= 100;  // 643.43
    int beltTaxRound = (int) beltTax; // 643
    double beltTaxRound2 = (double) beltTaxRound; //643.00
    beltTaxRound2 /= 100; // 6.43
    beltTax = beltTaxRound2;
    
    double totalPreTaxCost = pantsCost + beltCost + shirtCost;
    double salesTax = beltTax + pantsTax + shirtTax;
    double totalCost = totalPreTaxCost + salesTax;
    
    System.out.println("Pants total cost = " + pantsCost);
    System.out.println("Shirt total cost = " + shirtCost);
    System.out.println("Belt total cost = " + beltCost);
    System.out.println("Pants total tax = " + pantsTax);
    System.out.println("Shirt total tax = " + shirtTax);
    System.out.println("Belt total tax = " + beltTax);
    System.out.println("Total cost before tax = " + totalPreTaxCost);
    System.out.println("Total sales tax = " + salesTax);
    System.out.println("Total cost including tax = " + totalCost);
	}
}

