import java.util.Random;
public class CardGenerator
{
    public static void main(String[] args)
    {
      Random rnd = new Random();
      int cardNum = rnd.nextInt(52) + 1;  //1-52
      
      String suitName;
      
      if (cardNum <= 13) suitName = "Diamonds";
      else if (cardNum <= 26) suitName = "Clubs";
      else if (cardNum <= 39) suitName = "Hearts";
      else suitName = "Spades";
      
      int remainder = cardNum % 13;
      String prefix = "";
      
      switch (remainder)
      {
        case 1: 
          prefix = "Ace";
          break;
        case 11:
          prefix = "Jack";
          break;
        case 12:
          prefix = "Queen";
          break;
        case 0:
          prefix = "King";
          break;
        default:
          prefix = Integer.toString(remainder);
      }
      System.out.println(prefix + " of " + suitName);
      
    }
}