//Andrew Craig CSE 2 
// Carr
// 11/15

public class ArrayMethod {

   	public static void main(String[] args) {
     
      
      // Sections 6 -9
      int[] array0 = {1, 2, 3, 4, 5, 6};
      int[] array1 = {1, 2, 3, 4, 5, 6};
      int[] array2 = {1, 2, 3, 4, 5, 6};
      inverter(array0);
      print(array0);
      inverter2(array1);
      print(array1);
      int[] array3 = inverter2(array2);
      print(array3);
    }
  
  
  //copies array
    static int[] copy (int[] orig)
    {
      int[] x = new int[orig.length];
      for (int i = 0; i < orig.length; i++)
      {
        x[i] = orig[i];
      }
      return x;
    }
  //inverts the actual array
    static void inverter(int[] orig)
    {
      int[] origCopy = copy(orig);
      int x = 0;
      for (int i = orig.length - 1; i >= 0; i--)
      {
        orig[i] = origCopy[x];
        x++;
      }
    }
// makes a copy the inverses
    static int[] inverter2(int[] orig)
    {
      int[] origCopy = copy(orig);
      int x = 0;
      for (int i = origCopy.length - 1; i >= 0; i--)
      {
        origCopy[i] = orig[x];
        x++;
      }
      return origCopy;
    }
  //prints the array
    static void print(int[] orig)
    {
      for (int i = 0; i < orig.length; i++)
      {
        System.out.print(orig[i] + " ");
      }
      System.out.println("");
    }
  
}