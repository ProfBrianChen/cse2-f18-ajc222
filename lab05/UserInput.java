import java.util.Scanner;

public class UserInput
{
  
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    //Declare variables for our loops
    int courseNum;
    String departmentName;
    int meetingTimes;
    String startingTime;
    String instructorName;
    int studentNum;
    
    boolean cNNE = true;
    boolean dNNE = true;
    boolean mTNE = true;
    boolean sTNE = true;
    boolean iNNE = true;
    boolean sNNE = true;
    
    //Loops
    while (cNNE)
    {
      System.out.println("Enter course number (int)");
      
      if (scan.hasNextInt())
      {
        courseNum = scan.nextInt();
        System.out.println("Entered: " + courseNum);
        cNNE = false;
      }
      else
      {
        String garbage = scan.next();
      }
    }

    while (dNNE)
    {
      System.out.println("Enter department name (String)");
      
      if (scan.hasNext())
      {
        departmentName = scan.next();
        System.out.println("Entered: " + departmentName);
        dNNE = false;
      }
      else
      {
        String garbage = scan.next();
      }
    }

    while (mTNE)
    {
      System.out.println("Enter meeting times (int)");
      
      if (scan.hasNextInt())
      {
        meetingTimes = scan.nextInt();
        System.out.println("Entered: " + meetingTimes);
        mTNE = false;
      }
      else
      {
        String garbage = scan.next();
      }
    }
    
    while (sTNE)
    {
      System.out.println("Enter starting time (String)");
      
      if (scan.hasNext())
      {
        startingTime = scan.next();
        System.out.println("Entered: " + startingTime);
        sTNE = false;
      }
      else
      {
        String garbage = scan.next();
      }
    }
    
    while (iNNE)
    {
      System.out.println("Enter instructor name");
      
      if (scan.hasNext())
      {
        instructorName = scan.next();
        System.out.println("Entered: " + instructorName);
        iNNE = false;
      }
      else
      {
        String garbage = scan.next();
      }
    }
 
    while (sNNE)
    {
      System.out.println("Enter number of students (int)");
      
      if (scan.hasNextInt())
      {
        studentNum = scan.nextInt();
        System.out.println("Entered: " + studentNum);
        sNNE = false;
      }
      else
      {
        String garbage = scan.next();
      }
    }

    
    
  }
}