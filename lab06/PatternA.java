import java.util.Scanner;

public class PatternA
{
  
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    
    
    boolean linesNotEntered = true;
    int lines = 0;
    
    while (linesNotEntered)
    {
      System.out.println("Enter number of lines (int 1 - 10)");
      
      if (scan.hasNextInt())
      {
        int check = scan.nextInt();
        if (check > 0 && check < 11)
        {
          lines = check;
          System.out.println("Entered: " + lines);
          linesNotEntered = false;
        }
        
      }
      else
      {
        String garbage = scan.next();
      }
    }
    
    String j = "";
    for (int i = 1; i <= lines; i++)
    {
      j += i + " ";
      System.out.println(j);
    }
    
  } 
}