import java.util.Scanner;

public class PatternB
{
  
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    
    
    boolean linesNotEntered = true;
    int lines = 0;
    
    while (linesNotEntered)
    {
      System.out.println("Enter number of lines (int 1 - 10)");
      
      if (scan.hasNextInt())
      {
        int check = scan.nextInt();
        if (check > 0 && check < 11)
        {
          lines = check;
          System.out.println("Entered: " + lines);
          linesNotEntered = false;
        }
        
      }
      else
      {
        String garbage = scan.next();
      }
    }
    for (int i = lines; i >= 1; i--) // 6 - 1
    {
      System.out.println("");
      for (int j = 1; j <= i; j++)
      {
          System.out.print(j + " ");
      }
    }
    System.out.println("");
  } 
}