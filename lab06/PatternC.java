import java.util.Scanner;

public class PatternC
{
  
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    
    
    boolean linesNotEntered = true;
    int lines = 0;
    
    while (linesNotEntered)
    {
      System.out.println("Enter number of lines (int 1 - 10)");
      
      if (scan.hasNextInt())
      {
        int check = scan.nextInt();
        if (check > 0 && check < 11)
        {
          lines = check;
          System.out.println("Entered: " + lines);
          linesNotEntered = false;
        }
        
      }
      else
      {
        String garbage = scan.next();
      }
    }
    
    String num = "";
    
    for (int i = 1; i <= lines; i++) // 1 - 6
    {
      System.out.println("");
      num = i + num;
      for (int j = (lines - i); j >= 1; j--)
      {
          System.out.print(" ");
      }
      System.out.print(num);
    }
  } 
}