//Andrew Craig
//CSE 002
//Homework 04

import java.util.Random;
import java.util.Scanner;

public class CrapsSwitch
{
    public static void main(String[] args)
    {
      //Set up scanner
      Scanner scan = new Scanner(System.in);
      System.out.println("Would you like to manual input dice or randomly select them? a = manual, b = random");
      String choice = scan.next();
      
      //Initialize dice
      int dice1, dice2;
      dice1 = 1000;
      dice2 = 1000;
      
      //Handle the choice for either manual or random
        switch (choice)
        {
          case "a":
            while (dice1 < 1 || dice1 > 6)
            {
              System.out.println("Die One: Enter a number between 1 and 6");
              dice1 = scan.nextInt();
            }
          
            while (dice2 < 1 || dice2 > 6)
            {
             System.out.println("Die Two: Enter a number between 1 and 6");
             dice2 = scan.nextInt();
            }
            break;
      
          case "b":
            Random rnd = new Random();
            dice1 = rnd.nextInt(6) + 1;
            dice2 = rnd.nextInt(6) + 1;
            break;
        }
      System.out.println("Die One = " + dice1 + ", Die Two = " + dice2);
      
      //Determine the name of what we rolled
      String name;
      name = "";
      
      int diceEqual = (dice1 == dice2) ? 1 : 0;
      int diceSum = dice1 + dice2;
      
      switch(diceEqual)
      {
        case 1:
          switch(dice1)
          {
            case 1:
               name = "Snake Eyes";
            break;
            case 2:
               name = "Hard Four";
            break;
            case 3:
               name = "Hard Six";
            break;
            case 4:
               name = "Hard Eight";
            break;
            case 5:
               name = "Hard Ten";
            break;
            case 6:
               name = "Box Cars";
            break;
          }
        break;
          
        default:
          switch(diceSum)
                  {
                    case 3:
                       name = "Ace Deuce";
                    break;
                    case 4:
                       name = "Easy Four";
                    break;
                    case 5:
                       name = "Fever Five";
                    break;
                    case 6:
                       name = "Easy Six";
                    break;
                    case 7:
                       name = "Seven Out";
                    break;
                    case 8:
                       name = "Easy Eight";
                    break;
                    case 9:
                       name = "Nine";
                    break; 
                    case 10:
                       name = "Easy Ten";
                    break;
                    case 11:
                       name = "Yo-leven";
                    break;
                  }
                break;
              }
      
      System.out.println("You rolled a " + name);
      
      
    }
}