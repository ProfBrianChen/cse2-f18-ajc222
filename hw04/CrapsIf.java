//Andrew Craig
//CSE 002
//Homework 04

import java.util.Random;
import java.util.Scanner;

public class CrapsIf
{
    public static void main(String[] args)
    {
      //Set up scanner
      Scanner scan = new Scanner(System.in);
      System.out.println("Would you like to manual input dice or randomly select them? a = manual, b = random");
      String choice = scan.next();
      
      //Initialize dice
      int dice1, dice2;
      dice1 = 1000;
      dice2 = 1000;
      
      //Handle the choice for either manual or random
        if (choice.equals("a"))
        {
          while (dice1 < 1 || dice1 > 6)
          {
          System.out.println("Die One: Enter a number between 1 and 6");
          dice1 = scan.nextInt();
          }
          
          while (dice2 < 1 || dice2 > 6)
          {
          System.out.println("Die Two: Enter a number between 1 and 6");
          dice2 = scan.nextInt();
          }
        }
        else
        {
          Random rnd = new Random();
          dice1 = rnd.nextInt(6) + 1;
          dice2 = rnd.nextInt(6) + 1;
        }
      System.out.println("Die One = " + dice1 + ", Die Two = " + dice2);
      
      //Determine the name of what we rolled
      String name;
      name = "";
      
      if (dice1 == dice2)
      {
        if (dice1 == 1) name = "Snake Eyes";
        else if (dice1 == 2) name = "Hard Four";
        else if (dice1 == 3) name = "Hard Six";
        else if (dice1 == 4) name = "Hard Eight";
        else if (dice1 == 5) name = "Hard Ten";
        else if (dice1 == 6) name = "Box Cars";
      }
      else
      {
        if ((dice1 + dice2) == 3) name = "Acey Deuce";
        else if ((dice1 + dice2) == 4) name = "Easy Four";
        else if ((dice1 + dice2) == 5) name = "Fever Five";
        else  if ((dice1 + dice2) == 6) name = "Easy Six";
        else if ((dice1 + dice2) == 7) name = "Seven Out";
        else if ((dice1 + dice2) == 8) name = "Easy Eight";
        else if ((dice1 + dice2) == 9) name = "Nine";
        else if ((dice1 + dice2) == 10) name = "Easy Ten";
        else if ((dice1 + dice2) == 11) name = "Yo-leven";
      }
      
      System.out.println("You rolled a " + name);
      
      
    }
}
