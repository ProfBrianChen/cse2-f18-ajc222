import java.util.Random;

public class StoryGenerator {
  
  static String prot;
  
  public static void main (String[] args)
  {
    prot = getSubject();
    String sentence1 = "The " + getAdjective() + " " + getAdjective() + " " + prot + " " + getVerb() + " the " + getAdjective() + " " + getObject() + ".";
    String sentence2 = "This " + prot + " was astounded to see a " + getAdjective() + " " + getObject() + " next to itself.";
    String sentence3 = "Holy " + getAdjective() + " " + getObject() + "! " + " The " + getProtagionist() + " said in shock.";
    String sentence4 = "Luckily, a " + getAdjective() + " " + getSubject() + " " + getVerb() + " and fixed everything.";
    System.out.println(sentence1);
    System.out.println(sentence2);
    System.out.println(sentence3);
    System.out.println(sentence4);
    
  }
  
  public static String getProtagionist()
  {
    Random rnd = new Random();
    int s = rnd.nextInt(2);
    
    if (s == 0) return "It";
    else return prot;
    
  }
  
  public static String getAdjective()
  {  
     Random rnd = new Random();
    int word = rnd.nextInt(10);
    String myWord = "";
    
    switch(word)
    {
      case 0: myWord = "sexy";
        break;
      case 1: myWord = "ugly";
        break;
      case 2: myWord = "angry";
        break;
      case 3: myWord = "quick";
        break;
      case 4: myWord = "evil";
        break;
      case 5: myWord = "good";
        break;
      case 6: myWord = "true";
        break;
      case 7: myWord = "delightful";
        break;
      case 8: myWord = "real";
        break;
      default: myWord = "colorful";
        break;
    }
    return myWord;  
  }
 
  public static String getObject()
  { 
     Random rnd = new Random();
    int word = rnd.nextInt(10);
    String myWord = "";
    
    switch(word)
    {
      case 0: myWord = "pole";
        break;
      case 1: myWord = "tractor";
        break;
      case 2: myWord = "tar";
        break;
      case 3: myWord = "corn";
        break;
      case 4: myWord = "car";
        break;
      case 5: myWord = "computer";
        break;
      case 6: myWord = "zebra";
        break;
      case 7: myWord = "lollipop";
        break;
      case 8: myWord = "wheel";
        break;
      default: myWord = "fox";
        break;
    }
    return myWord;  
  }
  
  public static String getSubject()
  {  
     Random rnd = new Random();
    int word = rnd.nextInt(10);
    String myWord = "";
    
    switch(word)
    {
      case 0: myWord = "farner";
        break;
      case 1: myWord = "mail man";
        break;
      case 2: myWord = "hippo";
        break;
      case 3: myWord = "robber";
        break;
      case 4: myWord = "police man";
        break;
      case 5: myWord = "loser";
        break;
      case 6: myWord = "student";
        break;
      case 7: myWord = "queen";
        break;
      case 8: myWord = "underling";
        break;
      default: myWord = "McDonald's employee";
        break;
    }
    return myWord;  
  }
    
  public static String getVerb()
  {  
     Random rnd = new Random();
    int word = rnd.nextInt(10);
    String myWord = "";
    
    switch(word)
    {
      case 0: myWord = "stole";
        break;
      case 1: myWord = "jumped";
        break;
      case 2: myWord = "dove";
        break;
      case 3: myWord = "escaped";
        break;
      case 4: myWord = "destroyed";
        break;
      case 5: myWord = "questioned";
        break;
      case 6: myWord = "understood";
        break;
      case 7: myWord = "handed";
        break;
      case 8: myWord = "passed";
        break;
      default: myWord = "punched";
        break;
    }
    return myWord;  
  }
}