//Andrew Craig CSE2
//Carr
//11/15

import java.util.Scanner;
import java.util.Random;
  
  public class Shuffling{ 
    
    public static void main(String[] args) { 
      
      Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
      String[] suitNames={"C","H","S","D"};    
      String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
      String[] cards = new String[52]; 
      String[] hand = new String[5]; 
      int numCards = 5; 
      int again = 1; 
      int index = 51;
      for (int i=0; i<52; i++){ 
        cards[i]=rankNames[i%13]+suitNames[i/13]; 
        System.out.print(cards[i]+" "); 
      } 
      System.out.println();
      printArray(cards); 
      shuffle(cards); 
      printArray(cards); 

      while(again == 1){ 
      hand = getHand(cards,index,numCards); 
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
    //Gets a new hand
    static String[] getHand(String c[],int ind, int num)
    {
      String[] newHand = new String[num];
      for (int i = 0; i < newHand.length; i++)
      {
        if (ind < 0)
        {
          shuffle(c);
          ind = 51;
        }
        newHand[i] = c[ind];
        ind--;
      }
      return newHand;
    }
    //prints the array
    static void printArray(String x[])
    {
      for (int i = 0; i < x.length; i++)
      {
        System.out.print(x[i] + " ");
      }
      System.out.println("");
    }
    //shuffles the array
    static void shuffle(String x[])
    {
      Random rnd = new Random();
      for (int i = 0; i < 100; i++)
      {
        int spot = rnd.nextInt(x.length);
        String temp = x[0];
        x[0] = x[spot];
        x[spot] = temp;
      }
      
    }
    
}
