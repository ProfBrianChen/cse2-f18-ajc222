import java.util.Scanner;

public class EncryptedX {
	
	public static void main (String[] args)
	{
    //Set up scanner
		Scanner scan;
		scan = new Scanner(System.in);
    
    boolean numNotEntered = true;
    int num = 0;
    
    //Take a number only thru 0 to 100
    
    while (numNotEntered)
    {
      System.out.println("Enter a number 1 - 100");
      
      if (scan.hasNextInt())
      {
        int check = scan.nextInt();
        if (check >= 0 && check < 101)
        {
          num = check;
          System.out.println("Entered: " + num);
          numNotEntered = false;
        }
        
      }
      else
      {
        String garbage = scan.next();
      }
    }
    
    //Loops to draw the X
    
		for (int x = 0; x <= num; x++)
		{
			for (int y = 0; y <= num; y++)
			{
        
        //Select values for x
				if(y == x || y == (num - x))
				{
					System.out.print(" ");
				}
				else
				{
					System.out.print("*");
				}
			}
			System.out.println("");
		}
	}
}
