import java.util.Scanner;

public class WordTools {
  
  public static void main (String args[])
  {
    System.out.println("Enter a text");
    String text = sampleText();
    System.out.println("You entered: " + text);
  }
  
  static String sampleText()
  {
    Scanner scan = new Scanner(System.in);
    String store = "";
    while (scan.hasNext())
    {
      store += scan.next();
    }
    return store;
  }
  
}