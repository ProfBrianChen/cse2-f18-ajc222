import java.util.Scanner;

public class Convert
{

public static void main (String[] args)
{
  final double acreArea = 43560;
  final double mileLength = 5280;
 
  Scanner scan = new Scanner(System.in);
  System.out.println("Enter inches of rain");
  double rain = scan.nextDouble();
  System.out.println("Enter acres affected");
  double acres = scan.nextDouble();

  double volume = acreArea * (rain/12);  // volume in cubic feet
  double volumeMiles = volume /  Math.pow(mileLength, 2);
  System.out.print("Cubic miles affected: " + volumeMiles);
}
  
}