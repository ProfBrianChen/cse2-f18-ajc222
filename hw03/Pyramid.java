import java.util.Scanner;

public class Pyramid
{

public static void main (String[] args)
{
  Scanner scan = new Scanner(System.in);
  
  System.out.println("Enter base width");
  double bw = scan.nextDouble();
  System.out.println("Enter base length");
  double bl = scan.nextDouble();
  System.out.println("Enter base height");
  double height = scan.nextDouble();
  
  double volume = (bw * bl * height)/3;
  System.out.println("Volume = " + volume);
}
  
}