public class ArraysLab {

   	public static void main(String[] args) {
      
      int[] setInit = new int[100];
      for (int i = 0; i < 100; i++)
      {
        setInit[i] = (int)(Math.random() * 100);
        System.out.println(setInit[i]);
      }
      
      int[] setCheck = new int[100]; // Init array of zeros to count instances of each num
      for (int i = 0; i < 100; i++)
      {
        setCheck[i] = 0;
      }
      
      for (int h = 0; h < 100; h++)  // Go thru 0 - 99
      {
        for (int j = 0; j < 100; j++) // Go thru array
        {
          if (setInit[j] == h)
          {
            setCheck[h]++;
          }
        }
      }
      
      System.out.println("COUNT OF EACH NUMBER IN ARRAY:");
      
      for (int i = 0; i < 100; i++)
      {
        System.out.println(i + " occurs " + setCheck[i] + " times.");
      }
      
      
    }
  
}