import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;
public class Hw05{

  public static void main(String[] args)
  { 
    int fofk = 0, tofk = 0, tpair = 0, opair = 0;
    
    Scanner s = new Scanner(System.in);
    System.out.println("Enter number of loops");
    int loops = s.nextInt();
    
    for (int i = loops; i > 0; i--)
    {
        Random rnd = new Random();
        int card1, card2, card3, card4, card5;
      
      //generate our cards
        card1 = rnd.nextInt(52)+1;
        do
        {
          card2 = rnd.nextInt(52)+1;
        } while(card2 == card1);
        do
        {
          card3 = rnd.nextInt(52)+1;
        } while(card3 == card1 || card3 == card2);
        do
        {
          card4 = rnd.nextInt(52)+1;
        } while(card4 == card1 || card4 == card2 || card4 == card3);
        do
        {
          card5 = rnd.nextInt(52)+1;
        } while(card5 == card1 || card5 == card2 || card5 == card3 || card5 == card4);       
        //System.out.println(card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5); 
        
      
      //find out what type our cards are regardless of suit
         int num1 = card1 % 13;
         int num2 = card2 % 13;
         int num3 = card3 % 13;
         int num4 = card4 % 13;
         int num5 = card5 % 13;
      
         int[] duplicates = new int[13];
          
         for(int j = 0; j < 13; j++)
         {
           duplicates[j] = 0;
           if (num1 == j) duplicates[j] ++;
           if (num2 == j) duplicates[j] ++;
           if (num3 == j) duplicates[j] ++;
           if (num4 == j) duplicates[j] ++;
           if (num5 == j) duplicates[j] ++;
         }
      
          int pairCount = 0;
      
      
          //count duplicates
          for (int p = 0; p < duplicates.length - 1; p++)
          {
            if (duplicates[p] == 4) fofk++;
            if (duplicates[p] == 3) tofk++;
            if (duplicates[p] == 2) pairCount++;
             
          }
      
          if (pairCount == 1) opair++;
          else if (pairCount == 2) tpair++;
 
    }
    
    
        // calculate percentages
          double fofkPercent = (double) fofk / (double) loops;
          fofkPercent = (double) Math.round(fofkPercent * 1000d) / 1000d;
    
          double tofkPercent = (double) tofk / (double) loops;
          tofkPercent = (double) Math.round(tofkPercent * 1000d) / 1000d;
    
          double tpairPercent = (double) tpair / (double) loops;
          tpairPercent = (double) Math.round(tpairPercent * 1000d) / 1000d;

          double opairPercent = (double) opair / (double) loops;
          opairPercent = (double) Math.round(opairPercent * 1000d) / 1000d;
    
          System.out.println("Loops " + loops);
          System.out.println("Four of a kind " + fofkPercent);
          System.out.println("Three of a kind " + tofkPercent);
          System.out.println("Two pair " + tpairPercent);
          System.out.println("One pair " + opairPercent); 
  
  }
 
  
}